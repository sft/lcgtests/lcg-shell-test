include(ExternalProject)
include(CMakeParseArguments)

#---------------------------------------------------------------------------------------------------
# Helper function to substibute conditional code in arguments
#   o Expession like IF <condition> THEN <arguments1> [ELSE <arguments2> ] ENDIF gets
#     replaced by <arguments1> or <argummens2> depending on the conditioon
#---------------------------------------------------------------------------------------------------
function(LCG_replace_conditional_args outvar input)
  while(1)
    set(pattern)
    set(flag 0)
    set (idx 0) # incapsulate counter
    foreach(a IN LISTS input)
      if(a STREQUAL IF)
        set(flag 1)
        MATH (EXPR idx "${idx} + 1")
        list(APPEND pattern "${a}-${idx}")
      elseif(a STREQUAL ENDIF)
        list(APPEND pattern "${a}-${idx}")
        MATH (EXPR idx "${idx} - 1")
        if (idx EQUAL 0)
          break()
        endif()
      elseif(flag)
        if (a STREQUAL THEN)
          list (APPEND pattern "${a}-${idx}")
        elseif(a STREQUAL "ELSE")
          list (APPEND pattern "${a}-${idx}")
        else()
          list(APPEND pattern "${a}")
        endif()
      endif()
    endforeach()
    if(pattern)
      # Mark levels of IF statements: IF-1, THEN-1, ENDIF-1 ...
      if("${pattern}" MATCHES "IF-[0-9];(.+);THEN-\\1;(.+);ELSE-\\1;(.+);ENDIF-\\1")
        set(condition "${CMAKE_MATCH_1}")
        set(casetrue  "${CMAKE_MATCH_2}")
        set(casefalse "${CMAKE_MATCH_3}")
      elseif("${pattern}"  MATCHES "IF-[0-9];(.+);THEN-\\1;(.+);ENDIF-\\1")
        set(condition "${CMAKE_MATCH_1}")
        set(casetrue  "${CMAKE_MATCH_2}")
        set(casefalse "")
      else()
        message(FATAL_ERROR "Unsupported IF...THEN...ELSE..ENDIF construct: '${pattern}'")
      endif()
      if(${condition})
        set(value "${casetrue}")
      else()
        set(value "${casefalse}")
      endif()
      # Revert IF-# hack ...
      foreach (s IF THEN ELSE ENDIF)
        string(REGEX REPLACE "${s}-[0-9]" "${s}" pattern "${pattern}")
        string(REGEX REPLACE "${s}-[0-9]" "${s}" value "${value}")
      endforeach()
      string(REPLACE "${pattern}" "${value}" input "${input}")
    else()
      set(${outvar} "${input}" PARENT_SCOPE)
      return()
    endif()
  endwhile()
endfunction()

#-----------------------------------------------------------------------
# function LCG_add_test(<name> TEST_COMMAND cmd [arg1... ]
#                           [PRE_COMMAND cmd [arg1...]]
#                           [POST_COMMAND cmd [arg1...]]
#                           [OUTPUT outfile] [ERROR errfile]
#                           [WORKING_DIRECTORY directory]
#                           [ENVIRONMENT var1=val1 var2=val2 ...
#                           [DEPENDS test1 ...]
#                           [TIMEOUT seconds]
#                           [DEBUG]
#                           [SOURCE_DIR dir] [BINARY_DIR dir]
#                           [BUILD target] [PROJECT project]
#                           [BUILD_OPTIONS options]
#                           [PASSREGEX exp] [FAILREGEX epx]
#                           [LABELS label1 label2 ...])
#
function(LCG_add_test test)
  LCG_replace_conditional_args(ARGN "${ARGN}")
  cmake_parse_arguments(ARG
    "DEBUG"
    "TIMEOUT;BUILD;OUTPUT;ERROR;SOURCE_DIR;BINARY_DIR;PROJECT;PASSREGEX;FAILREGEX;WORKING_DIRECTORY"
    "TEST_COMMAND;PRE_COMMAND;POST_COMMAND;ENVIRONMENT;DEPENDS;LABELS;BUILD_OPTIONS"
    ${ARGN})

  if(NOT CMAKE_GENERATOR MATCHES Makefiles)
    set(_cfg $<CONFIGURATION>/)
  endif()

  #- Handle TEST_COMMAND argument
  list(LENGTH ARG_TEST_COMMAND _len)
  if(_len LESS 1)
    if(NOT ARG_BUILD)
      message(FATAL_ERROR "LCG_ADD_TEST: command is mandatory (without build)")
    endif()
  else()
    list(GET ARG_TEST_COMMAND 0 _prg)
    list(REMOVE_AT ARG_TEST_COMMAND 0)
    if(NOT IS_ABSOLUTE ${_prg})
      #set(_prg ${CMAKE_CURRENT_BINARY_DIR}/${_cfg}${_prg})
    elseif(EXISTS ${_prg})
    else()
      get_filename_component(_path ${_prg} PATH)
      get_filename_component(_file ${_prg} NAME)
      set(_prg ${_path}/${_cfg}${_file})
    endif()
    set(_cmd ${_prg} ${ARG_TEST_COMMAND})
    string(REPLACE ";" "#" _cmd "${_cmd}")
  endif()

  set(_command ${CMAKE_COMMAND} -DTST=${test} -DCMD:STRING=${_cmd})

  #- Handle PRE and POST commands
  if(ARG_PRE_COMMAND)
    set(_pre ${ARG_PRE_COMMAND})
    string(REPLACE ";" "#" _pre "${_pre}")
    set(_command ${_command} -DPRE:STRING=${_pre})
  endif()
  if(ARG_POST_COMMAND)
    set(_post ${ARG_POST_COMMAND})
    string(REPLACE ";" "#" _post "${_post}")
    set(_command ${_command} -DPOST:STRING=${_post})
  endif()

  #- Handle OUTPUT, ERROR, DEBUG arguments
  if(ARG_OUTPUT)
    set(_command ${_command} -DOUT=${ARG_OUTPUT})
  endif()

  if(ARG_ERROR)
    set(_command ${_command} -DERR=${ARG_ERROR})
  endif()

  if(ARG_DEBUG)
    set(_command ${_command} -DDBG=ON)
  endif()

  if(ARG_WORKING_DIRECTORY)
    set(_command ${_command} -DCWD=${ARG_WORKING_DIRECTORY})
  endif()

  if(ARG_TIMEOUT)
    set(_command ${_command} -DTIM=${ARG_TIMEOUT})
  endif()

  #- Handle ENVIRONMENT argument
  if(ARG_ENVIRONMENT)
    string(REPLACE ";" "#" _env "${ARG_ENVIRONMENT}")
    string(REPLACE "=" "@" _env "${_env}")
    set(_command ${_command} -DENV=${_env})
  endif()

  #- Locate the test driver
  set(_driver ${CMAKE_SOURCE_DIR}/cmake/scripts/TestDriver.cmake)
  if(NOT EXISTS ${_driver})
    message(FATAL_ERROR "LCG_ADD_TEST: TestDriver.cmake not found!")
  endif()
  set(_command ${_command} -DTESTLOGDIR=${TESTLOGDIR} -P ${_driver})

  #- Now we can actually add the test
  if(ARG_BUILD)
    if(ARG_SOURCE_DIR)
      if(NOT IS_ABSOLUTE ${ARG_SOURCE_DIR})
        set(ARG_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_SOURCE_DIR})
      endif()
    else()
      set(ARG_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    endif()
    if(ARG_BINARY_DIR)
      if(NOT IS_ABSOLUTE ${ARG_BINARY_DIR})
        set(ARG_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/${ARG_BINARY_DIR})
      endif()
    else()
      set(ARG_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR})
    endif()
    if(NOT ARG_PROJECT)
       if(NOT PROJECT_NAME STREQUAL "LCGSoft")
         set(ARG_PROJECT ${PROJECT_NAME})
       else()
         set(ARG_PROJECT ${ARG_BUILD})
       endif()
    endif()
    add_test(NAME ${test} COMMAND ${CMAKE_CTEST_COMMAND}
      --output-log ${TESTLOGDIR}/${test}.log
      --build-and-test  ${ARG_SOURCE_DIR} ${ARG_BINARY_DIR}
      --build-generator ${CMAKE_GENERATOR}
      --build-makeprogram ${CMAKE_MAKE_PROGRAM}
      --build-target ${ARG_BUILD}
      --build-project ${ARG_PROJECT}
      --build-config $<CONFIGURATION>
      --build-noclean
      --build-options ${ARG_BUILD_OPTIONS}
      --test-command ${_command} )
    set_property(TEST ${test} PROPERTY ENVIRONMENT Geant4_DIR=${CMAKE_BINARY_DIR})
    if(ARG_FAILREGEX)
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION "error:|(${ARG_FAILREGEX})")
    else()
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION "error:")
    endif()
  else()
    add_test(NAME ${test} COMMAND ${_command})
    if(ARG_FAILREGEX)
      set_property(TEST ${test} PROPERTY FAIL_REGULAR_EXPRESSION ${ARG_FAILREGEX})
    endif()
  endif()

  #- Handle TIMOUT and DEPENDS arguments
  if(ARG_TIMEOUT)
    set_property(TEST ${test} PROPERTY TIMEOUT ${ARG_TIMEOUT})
  endif()

  if(ARG_DEPENDS)
    if(${targetname}-dependencies)
      set_property(TEST ${test} PROPERTY DEPENDS ${ARG_DEPENDS})
    endif()
  endif()

  if(ARG_PASSREGEX)
    set_property(TEST ${test} PROPERTY PASS_REGULAR_EXPRESSION ${ARG_PASSREGEX})
  endif()

  if(ARG_LABELS)
    set_property(TEST ${test} PROPERTY LABELS ${ARG_LABELS})
  else()
    set_property(TEST ${test} PROPERTY LABELS Nightly)
  endif()
endfunction()

#-----------------------------------------------------------------------
# function LCG_add_multiple_tests(<pre-name> WILDCARD wildcard
#                           [TEST_COMMAND cmd [arg1... ]]
#                           [PRE_COMMAND cmd [arg1...]]
#                           [POST_COMMAND cmd [arg1...]]
#                           [OUTPUT outfile] [ERROR errfile]
#                           [WORKING_DIRECTORY directory]
#                           [ENVIRONMENT var1=val1 var2=val2 ...
#                           [DEPENDS test1 ...]
#                           [TIMEOUT seconds]
#                           [DEBUG]
#                           [SOURCE_DIR dir] [BINARY_DIR dir]
#                           [BUILD target] [PROJECT project]
#                           [BUILD_OPTIONS options]
#                           [PASSREGEX exp] [FAILREGEX epx]
#                           [LABELS label1 label2 ...])
#
function(LCG_add_multiple_tests test)
  LCG_replace_conditional_args(ARGN "${ARGN}")
  cmake_parse_arguments(ARG
    "DEBUG"
    "TIMEOUT;BUILD;OUTPUT;ERROR;SOURCE_DIR;BINARY_DIR;PROJECT;PASSREGEX;FAILREGEX;WORKING_DIRECTORY"
    "WILDCARD;TEST_COMMAND;PRE_COMMAND;POST_COMMAND;ENVIRONMENT;DEPENDS;LABELS;BUILD_OPTIONS"
    ${ARGN})

  if(NOT CMAKE_GENERATOR MATCHES Makefiles)
    set(_cfg $<CONFIGURATION>/)
  endif()

  #- Handle TEST_COMMAND argument
  list(LENGTH ARG_WILDCARD _len)
  if(_len LESS 1)
      message(FATAL_ERROR "LCG_add_multiple_tests: wildcard is mandatory (without build)")
  endif()
  
  set(FORWARDED_ARGS "")
  if(ARG_PRE_COMMAND)
     list(APPEND FORWARDED_ARGS " PRE_COMMAND ${ARG_PRE_COMMAND} ")
  endif()
  if(ARG_POST_COMMAND)
     list(APPEND FORWARDED_ARGS " POST_COMMAND ${ARG_POST_COMMAND} ")
  endif()
  if(ARG_OUTPUT)
     list(APPEND FORWARDED_ARGS " OUTPUT ${ARG_OUTPUT} ")
  endif()
  if(ARG_ERROR)
     list(APPEND FORWARDED_ARGS " ERROR ${ARG_ERROR} ")
  endif()
  if(ARG_WORKING_DIRECTORY)
     list(APPEND FORWARDED_ARGS " WORKING_DIRECTORY ${ARG_WORKING_DIRECTORY} ")
  endif()
  if(ARG_ENVIRONMENT)
     list(APPEND FORWARDED_ARGS "ENVIRONMENT ${ARG_ENVIRONMENT} ")
  endif()
  if(ARG_DEPENDS)
     list(APPEND FORWARDED_ARGS "DEPENDS ${ARG_DEPENDS} ")
  endif()
  if(ARG_TIMEOUT)
     list(APPEND FORWARDED_ARGS "TIMEOUT ${ARG_TIMEOUT} ")
  endif()
  if(ARG_DEBUG)
     list(APPEND FORWARDED_ARGS "DEBUG ${ARG_DEBUG} ")
  endif()
  if(ARG_SOURCE_DIR)
     list(APPEND FORWARDED_ARGS "SOURCE_DIR ${ARG_SOURCE_DIR} ")
  endif()
  if(ARG_BINARY_DIR)
     list(APPEND FORWARDED_ARGS "BINARY_DIR ${ARG_BINARY_DIR} ")
  endif()
  if(ARG_BUILD)
     list(APPEND FORWARDED_ARGS "BUILD ${ARG_BUILD} ")
  endif()
  if(ARG_PROJECT)
     list(APPEND FORWARDED_ARGS "PROJECT ${ARG_PROJECT} ")
  endif()
  if(ARG_BUILD_OPTIONS)
     list(APPEND FORWARDED_ARGS "BUILD_OPTIONS ${ARG_BUILD_OPTIONS} ")
  endif()
  if(ARG_PASSREGEX)
     list(APPEND FORWARDED_ARGS "PASSREGEX ${ARG_PASSREGEX} ")
  endif()
  if(ARG_FAILREGEX)
     list(APPEND FORWARDED_ARGS "FAILREGEX ${ARG_FAILREGEX} ")
  endif()
  if(ARG_LABELS)
     list(APPEND FORWARDED_ARGS "LABELS ${ARG_LABELS} ")
  endif()

  if(FORWARDED_ARGS)
      message("Can occur problem with arguments")
  endif()

  set(_command ${CMAKE_COMMAND} -DTST=${test} -DCMD:STRING=${_cmd})
  file(GLOB _files ${CMAKE_CURRENT_SOURCE_DIR}/${ARG_WILDCARD})
  foreach(_file ${_files})
      file(RELATIVE_PATH _testname ${CMAKE_CURRENT_SOURCE_DIR} ${_file})
      LCG_add_test(${test}_${_testname} TEST_COMMAND ${ARG_TEST_COMMAND} ${_file} ${FORWARDED_ARGS})
  endforeach()
endfunction()


#-----------------------------------------------------------------------
# function LCG_add_templated_tests(<name> FILEPATH filepath
#                           [TEST_COMMAND cmd [arg1... ]]
#                           [LEVEL num]
#                           [PRE_COMMAND cmd [arg1...]]
#                           [POST_COMMAND cmd [arg1...]]
#                           [OUTPUT outfile] [ERROR errfile]
#                           [WORKING_DIRECTORY directory]
#                           [ENVIRONMENT var1=val1 var2=val2 ...
#                           [DEPENDS test1 ...]
#                           [TIMEOUT seconds]
#                           [DEBUG]
#                           [SOURCE_DIR dir] [BINARY_DIR dir]
#                           [BUILD target] [PROJECT project]
#                           [BUILD_OPTIONS options]
#                           [PASSREGEX exp] [FAILREGEX epx]
#                           [LABELS label1 label2 ...])
#
# The content of the file is read by line by line
# It fills the variables $1 ... $n, there n-1 is the number of # before the line
# Templated fields: NAME, TEST_COMMAND


function(LCG_add_templated_tests test)
  LCG_replace_conditional_args(ARGN "${ARGN}")
  cmake_parse_arguments(ARG
    "DEBUG"
    "TIMEOUT;BUILD;OUTPUT;ERROR;SOURCE_DIR;BINARY_DIR;PROJECT;PASSREGEX;FAILREGEX;WORKING_DIRECTORY"
    "FILEPATH;LEVEL;TEST_COMMAND;PRE_COMMAND;POST_COMMAND;ENVIRONMENT;DEPENDS;LABELS;BUILD_OPTIONS"
    ${ARGN})

  if(NOT CMAKE_GENERATOR MATCHES Makefiles)
    set(_cfg $<CONFIGURATION>/)
  endif()

  #- Handle TEST_COMMAND argument
  list(LENGTH ARG_FILEPATH _len)
  if(_len LESS 1)
      message(FATAL_ERROR "LCG_add_templated_tests: filepath is mandatory (without build)")
  endif()
  list(LENGTH ARG_LEVEL _len)
  if(_len LESS 1)
    set(ARG_LEVEL 1)
  endif()
  
  set(FORWARDED_ARGS "")
  if(ARG_PRE_COMMAND)
      string(REPLACE ";" " " _ARG "${ARG_PRE_COMMAND}")
     list(APPEND FORWARDED_ARGS " PRE_COMMAND ${_ARG} ")
  endif()
  if(ARG_POST_COMMAND)
      string(REPLACE ";" " " _ARG "${ARG_POST_COMMAND}")
     list(APPEND FORWARDED_ARGS " POST_COMMAND ${_ARG} ")
  endif()
  if(ARG_OUTPUT)
     list(APPEND FORWARDED_ARGS " OUTPUT ${ARG_OUTPUT} ")
  endif()
  if(ARG_ERROR)
     list(APPEND FORWARDED_ARGS " ERROR ${ARG_ERROR} ")
  endif()
  if(ARG_WORKING_DIRECTORY)
     list(APPEND FORWARDED_ARGS " WORKING_DIRECTORY ${ARG_WORKING_DIRECTORY} ")
  endif()
  if(ARG_ENVIRONMENT)
     list(APPEND FORWARDED_ARGS "ENVIRONMENT ${ARG_ENVIRONMENT} ")
  endif()
  if(ARG_DEPENDS)
      string(REPLACE ";" " " _ARG "${ARG_DEPENDS}")
     list(APPEND FORWARDED_ARGS "DEPENDS ${_ARG} ")
  endif()
  if(ARG_TIMEOUT)
     list(APPEND FORWARDED_ARGS "TIMEOUT ${ARG_TIMEOUT} ")
  endif()
  if(ARG_DEBUG)
     list(APPEND FORWARDED_ARGS "DEBUG ${ARG_DEBUG} ")
  endif()
  if(ARG_SOURCE_DIR)
     list(APPEND FORWARDED_ARGS "SOURCE_DIR ${ARG_SOURCE_DIR} ")
  endif()
  if(ARG_BINARY_DIR)
     list(APPEND FORWARDED_ARGS "BINARY_DIR ${ARG_BINARY_DIR} ")
  endif()
  if(ARG_BUILD)
      string(REPLACE ";" " " _ARG "${ARG_BUILD}")
     list(APPEND FORWARDED_ARGS "BUILD ${_ARG} ")
  endif()
  if(ARG_PROJECT)
      string(REPLACE ";" " " _ARG "${ARG_PRE_COMMAND}")
     list(APPEND FORWARDED_ARGS "PROJECT ${_ARG} ")
  endif()
  if(ARG_BUILD_OPTIONS)
      string(REPLACE ";" " " _ARG "${ARG_PRE_COMMAND}")
     list(APPEND FORWARDED_ARGS "BUILD_OPTIONS ${_ARG} ")
  endif()
  if(ARG_PASSREGEX)
     list(APPEND FORWARDED_ARGS "PASSREGEX ${ARG_PASSREGEX} ")
  endif()
  if(ARG_FAILREGEX)
     list(APPEND FORWARDED_ARGS "FAILREGEX ${ARG_FAILREGEX} ")
  endif()
  if(ARG_LABELS)
     list(APPEND FORWARDED_ARGS "LABELS ${ARG_LABELS} ")
  endif()

  if(FORWARDED_ARGS)
      message("Can occur problem with arguments")
  endif()

  set(_command ${CMAKE_COMMAND} -DTST=${test} -DCMD:STRING=${_cmd})

  file(READ "${ARG_FILEPATH}" _filecontent)
  string(REGEX REPLACE "\n" ";" _filecontent "${_filecontent}")
  foreach(_line ${_filecontent})
    string(REGEX REPLACE "#" ";" _line "${_line}")
    list(LENGTH _line _level)
    MATH(EXPR _lvl "${_level}-1")
    foreach(_col RANGE 0 ${_lvl})
      list(GET _line ${_col} _arg)
      MATH(EXPR col "${_col}+1")
      if(_arg) 
        set(level${col} ${_arg})
        if(${col} EQUAL ${ARG_LEVEL})
            set(_templated_name "${test}")
            set(_templated_cmd "${ARG_TEST_COMMAND}")
            foreach(_iter RANGE 1 ${ARG_LEVEL})
                string(REPLACE "$${_iter}" "${level${_iter}}" _templated_name "${_templated_name}")
                string(REPLACE "$${_iter}" "${level${_iter}}" _templated_cmd "${_templated_cmd}")
            endforeach()
            #string(REGEX REPLACE "[\t ]+" ";" nc "${_templated_cmd}")
            LCG_add_test(${_templated_name} TEST_COMMAND ${_templated_cmd} ${FORWARDED_ARGS})
        endif()
      endif()
    endforeach()
  endforeach()
endfunction()



