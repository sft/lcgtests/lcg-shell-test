#!/bin/bash -e
SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SHEL=$1;shift;
VIEW=$1;shift;
if [ $SHEL == 'bash' ]; then
  bash -xev $SCRIPTDIR/runme.sh "$VIEW/setup.sh"
else
  if [ $SHEL == 'zsh' ]; then
    zsh -xev $SCRIPTDIR/runme.sh "$VIEW/setup.sh"
  else
    # '-f' disables init scripts
    tcsh -xevf $SCRIPTDIR/runme.csh "$VIEW/setup.csh"
  fi
fi
